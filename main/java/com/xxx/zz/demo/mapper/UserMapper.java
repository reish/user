package com.xxx.zz.demo.mapper;

import com.xxx.zz.demo.pojo.Params;
import com.xxx.zz.demo.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;

public interface UserMapper {

    @Select("select * from user")
    List<User> findAll();

    @Insert("insert into user (username, birthDate) values (#{user.username}, #{user.birthDate})")
    int insertUser(@Param("user")User user);

    @Select("select * from user where username like concat('%',#{params.username},'%') and birthDate between #{params.datepre} and #{params.dateaft}")
    List<User> findByParams(@Param("params")Params params);
}
