package com.xxx.zz.demo.pojo;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
public class User implements Serializable {
    private Integer userId;
    private String username;
    private Date birthDate;
}
