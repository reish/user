package com.xxx.zz.demo.pojo;

import lombok.Data;

import java.util.Date;

/*
*  这个类用来接收前端发送的搜索参数
*
*/
@Data
public class Params {
    private String username;
    private Date datepre;
    private Date dateaft;
}
