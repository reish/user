package com.xxx.zz.demo.utils;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {
    //1 java.util.Date->java.sql.Date;
    public static java.sql.Date toSqlDate(java.util.Date date){
        return new java.sql.Date(date.getTime());
    }
    //2 java.sql.Date->java.util.Date;

    public static java.util.Date toUtilDate(java.sql.Date date){
        return new java.util.Date(date.getTime());
    }
    //3 String->java.util.Date
    public static java.util.Date toUtilDate(String dateStr){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    //4 java.util.Date->String

    public static String toUtilDate(java.util.Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    //1 java.util.Date->java.sql.Date;
    public static java.sql.Timestamp toSqlTimeStamp(java.util.Date date){
        return new java.sql.Timestamp(date.getTime());
    }
}
