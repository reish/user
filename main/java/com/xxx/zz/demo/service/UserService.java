package com.xxx.zz.demo.service;

import com.xxx.zz.demo.pojo.Params;
import com.xxx.zz.demo.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface UserService {

    List<User> findAll();

    int insertUser(User user);

    List<User> findByParams(@Param("params")Params params);
    //List<User> findByParams(String username, Date datepre, Date dateaft);

}
