package com.xxx.zz.demo.service.impl;

import com.xxx.zz.demo.mapper.UserMapper;
import com.xxx.zz.demo.pojo.Params;
import com.xxx.zz.demo.pojo.User;
import com.xxx.zz.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper usermapper;

    public List<User> findAll() {
        return usermapper.findAll();
    }


    public int insertUser(User user) {
        return usermapper.insertUser(user);
    }


    public List<User> findByParams(Params params) {
        return usermapper.findByParams(params);
    }
}
