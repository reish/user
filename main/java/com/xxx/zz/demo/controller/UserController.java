package com.xxx.zz.demo.controller;

import com.xxx.zz.demo.pojo.Params;
import com.xxx.zz.demo.pojo.User;
import com.xxx.zz.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    UserService userservice;

    @RequestMapping("/findAll")
    @ResponseBody
    List<User> findAll() {
        List<User> list = userservice.findAll();
        for(User user:list){
            System.out.println(user);
        }
        return list;
    }

    @RequestMapping("/insertUser")
    @ResponseBody
    int insertUser(@RequestBody User user) {
        //System.out.println(user);
        int result = userservice.insertUser(user);
        if(result != 1){
            System.out.println("插入失败, 请检查数据");
        }else{
            System.out.println("插入成功");
        }
        return result;
    }

    @RequestMapping("/insertUsers")
    @ResponseBody
    int insertUsers(@RequestBody List<User> list){
        int result = 0;
        for(User user:list){
            result += userservice.insertUser(user);
        }
        if(result != list.size()){
            System.out.println("有数据未能插入成功");
        }else{
            System.out.println("数据全部插入成功");
        }
        return result;
    }

    @RequestMapping("/findByParams")
    @ResponseBody
    List<User> findByParams(@RequestBody Params params){
        //System.out.println(params.getUsername()+"##"+params.getDatepre()+"$$"+params.getDateaft());
        List<User> list = userservice.findByParams(params);
        for(User user:list){
            System.out.println(user);
        }
        return list;
    }

}
